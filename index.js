const express = require('express');
const app = express();
const { v4: uuidv4 } = require('uuid');
const fs = require('fs');
const path = require('path');
require('dotenv').config();

const htmlPath = path.join(__dirname, 'index.html');
const jsonPath = path.join(__dirname, 'data.json');
const logPath = path.join(__dirname, 'logs.log');

app.use((request, response, next) => {
    let url = request.url;
    let method = request.method;
    let time = new Date();
    const log = `"Method:" ${method} "Time:" ${time} "Url:" ${url} \n`;
    fs.appendFile('./logs.log', log, (error) => {
        if (error) {
            console.error(error);
        } else {
            console.log('Data logged successfully');
            next();
        }
    });
});

app.get('/html', (request, response) => {
    response.sendFile(htmlPath);
});

app.get('/json', (request, response) => {
    response.sendFile(jsonPath);
});

app.get('/uuid', (request, response) => {
    let uuidData = JSON.stringify({
        uuid: uuidv4()
    });
    response.json(uuidData);
});

app.get('/status/:id', (request, response) => {
    const statusCode = Number(request.params.id);
    if (statusCode >= 100 && statusCode < 200 || statusCode > 599) {
        response.send("Enter correct status code");
    } else {
        response.status(statusCode).send(`Responding with status-code:${statusCode}`);
    }
});

app.get('/delay/:time', (request, response) => {
    const delay = Number(request.params.time);
    setTimeout(() => {
        response.send(`Getting the response after ${delay} seconds`);
    }, delay * 1000);
});

app.get('/logs', (request, response) => {
    response.sendFile(logPath);
});

app.use((request, response) => {
    response.status(404).send("Server could not found the requested web-page");
});

const port = process.env.PORT || 8000;

app.listen(port, (error) => {
    if (error) {
        console.error(error);
        return;
    } else {
        console.log("Server is up and running on port:", port);
    }
});